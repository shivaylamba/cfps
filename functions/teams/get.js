const process = require("process");

const {
  Index,
  Match,
  Paginate,
  Get,
  Client,
  Ref,
  Collection,
  Call,
  Function,
  Select,
} = require("faunadb");

const client = new Client({
  secret: process.env.FAUNADB_SERVER_SECRET,
});

const all = async (userId) => {
  console.log("Read all user Teams");
  if (userId) {
    try {
      console.log(`Get all user teams for ${userId}`);
      const response = await client.query(
        Paginate(Match(Index("teams_members"), userId))
      );
      const itemRefs = response.data;
      const getAllItemsDataQuery = itemRefs.map((ref) => Get(ref));
      const ret = await client.query(getAllItemsDataQuery);
      const body = JSON.stringify(ret);
      return {
        statusCode: 200,
        body,
      };
    } catch (error) {
      console.log("error", error);
      return {
        statusCode: 400,
        body: JSON.stringify(error),
      };
    }
  } else {
    console.error("No userId when getting all user teams");
    return {
      statusCode: 404,
      body: JSON.stringify({ error: "Not Found" }),
    };
  }
};

const one = async (userId, ref) => {
  console.log(`User ${userId} get team ${ref}`);

  if (userId) {
    try {
      const response = await client.query(Get(Ref(Collection("teams"), ref)));
      const memberDetails = await client.query(Call(Function("members_by_teamRef"), ref));
      const body = JSON.stringify({
        ...response.data,
        memberDetails,
      });
      if (response.data.userId === userId) {
        return {
          statusCode: 200,
          body,
        };
      } else {
        if (response.data.members.includes(userId)) {
          return {
            statusCode: 200,
            body,
          };
        } else {
          return {
            statusCode: 401,
            body: JSON.stringify({
              message: `Team ${ref} is not associated with user ${userId}`,
            }),
          };
        }
      }
    } catch (error) {
      console.log("error", error);
      return {
        statusCode: 400,
        body: JSON.stringify(error),
      };
    }
  } else {
    return {
      statusCode: 404,
      body: JSON.stringify({ error: "Not Found" }),
    };
  }
};

const cfps = async (userId, ref) => {
  console.log(`User ${userId} get team ${ref} CFPs`);

  if (userId) {
    try {
      const response = await client.query(Get(Ref(Collection("teams"), ref)));
      const teamCFPsRefs = await client.query(Paginate(Match(Index('cfps_by_teamid'), response.data.teamId)));
      const itemRefs = teamCFPsRefs.data
      const getAllItemsDataQuery = itemRefs.map((ref) => Get(ref))
      const ret = await client.query(getAllItemsDataQuery);
      body = JSON.stringify(ret)

      if (response.data.userId === userId) {
        return {
          statusCode: 200,
          body,
        };
      } else {
        if (response.data.members.includes(userId)) {
          return {
            statusCode: 200,
            body,
          };
        } else {
          return {
            statusCode: 401,
            body: JSON.stringify({
              message: `Team ${ref} is not associated with user ${userId}`,
            }),
          };
        }
      }
    } catch (error) {
      console.log("error", error);
      return {
        statusCode: 400,
        body: JSON.stringify(error),
      };
    }
  } else {
    return {
      statusCode: 404,
      body: JSON.stringify({ error: "Not Found" }),
    };
  }
}

module.exports = { all, one, cfps };
