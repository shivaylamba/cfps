const process = require("process");
const crypto = require("crypto")

const {
  query,
  Create,
  Collection,
  Paginate,
  Select,
  Get,
  Client,
} = require("faunadb");

const client = new Client({
  secret: process.env.FAUNADB_SERVER_SECRET,
});

const newTeam = async (event, userId) => {
  const data = JSON.parse(event.body);
  console.log("Function newTeam", data);
  const item = {
    data: {
      teamname: data.teamname,
      teamId: crypto.randomUUID(),
      stripeCustId: "",
      subscribed: false,
      creatorId: userId,
      owners: [userId],
      members: [userId],
    },
  };

  // TODO: Stripe integration #66

  try {
    const response = await client.query(Create(Collection("teams"), item));
    console.log("Success", response);
    return {
      statusCode: 200,
      body: JSON.stringify(response),
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 400,
      body: JSON.stringify(error),
    };
  }
};

module.exports = { newTeam };
