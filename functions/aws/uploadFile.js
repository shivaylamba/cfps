const AWS = require('aws-sdk');
const process = require('process');

const s3 = new AWS.S3({
    accessKeyId: process.env.AWS_BUCKET_KEY_ID,
    secretAccessKey: process.env.AWS_BUCKET_SECRET_ACCESS_KEY
})

const bucketName = process.env.AWS_BUCKET_NAME;
const getRandomFilename = () => require("crypto").randomBytes(10).toString('hex')

const one = async (userId, event) => {
    if (userId) {
        try {
            contentType = event.queryStringParameters.contentType
            if (contentType) {
                if (contentType.indexOf('image') > -1) {
                    const fileName = `${userId}-${getRandomFilename()}.${contentType.substr(contentType.indexOf('/') + 1)}`
                    const signedURL = s3.getSignedUrl("putObject", {
                        Bucket: bucketName,
                        Key: fileName,
                        ContentType: contentType,
                        ACL: "public-read"
                    })
                    return {
                        statusCode: 200,
                        body: JSON.stringify({ url: signedURL, fileName: fileName })
                    }
                } else {
                    return {
                        statusCode: 400,
                        body: JSON.stringify({ message: 'contentType must be an image' }),
                    }
                }
            } else {
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: 'contentType missing' }),
                }
            }
        } catch (error) {
            console.log('error', error)
            return {
                statusCode: 400,
                body: JSON.stringify(error),
            }
        }
    } else {
        console.log("unauthorized");
        return {
            statusCode: 403,
            body: JSON.stringify({ message: 'You are not authorized to perform that action'})
        }
    }
}

module.exports = { one }