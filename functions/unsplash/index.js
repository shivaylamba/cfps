const search = require("./search");

const handler = async (event, context) => {
  console.log("Unsplash API");

  try {
    const path = event.path.replace(/\.netlify\/functions\/[^/]+/, "");
    const segments = path.split("/").filter(Boolean);

    const { user, identity } = context.clientContext;
    let userId = "";
    if (user) {
      if (user.sub) {
        userId = user.sub;
        switch (event.httpMethod) {
          case "GET":
            if (segments.length === 0) return search.byString(event, userId);
            if (segments.length === 1) {
              const [id] = segments;
              return search.download(event, userId, id);
            }
          default:
            console.log("Where is my large automobile?");
            break;
        }
      }
    }
  } catch (error) {
    console.log("General error in function");
    console.log("error", error);
    return {
      statusCode: 500,
      body: `There was an error ${error}`,
    };
  }

  return {
    statusCode: 400,
    body: JSON.stringify({ message: `Missing user` })
  };
};

module.exports = { handler };
