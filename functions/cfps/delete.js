const process = require('process');

const { Get, Client, Ref, Collection, Delete } = require('faunadb')

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const one = async (userId, ref) => {
    console.log(`User ${userId} delete CFP ${ref}`);

    try {
        const thisCFP = await client.query(Get(Ref(Collection('cfps'), ref)));

        if (thisCFP.data.userId === userId) {
            const response = await client.query(Delete(Ref(Collection('cfps'), ref)));
            console.log('success', response);
            return {
                statusCode: 200,
                body: JSON.stringify(response)
            }
        } else {
            return {
                statusCode: 401,
                body: JSON.stringify({ message: `CFP ${ref} is not associated with user ${userId}`})
            }
        }


    } catch (error) {
        console.log('error', error)
        return {
            statusCode: 400,
            body: JSON.stringify(error),
        }
    }
}

module.exports = { one }