const process = require('process');
const postmark = require("postmark");

const mailClient = new postmark.ServerClient(process.env.POSTMARK_API_KEY);


const { Index, Match, Create, Get, Client, Ref, Collection, Select, Call, Function } = require('faunadb')

const dbClient = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const welcome = async (userId) => {
    // Check if email has been sent
    try {
        const thisRef = await dbClient.query(
            Select(["ref"], Get(Match(Index("emails_user_type"), userId, "welcome")))
        );
        // No email to send
        return false;
    } catch (error) {
        if (error.requestResult.statusCode === 404) {
            // Welcome email not sent, let's send it
            // Get user
            const thisUser = await dbClient.query(Get(Match(Index("users_userid"), userId)));
            const toEmail = thisUser.data.email;
            const toName = thisUser.data.user_metadata?.full_name || 'User';

            mailClient.sendEmailWithTemplate({
                "From": "support@liscioapps.com",
                "To": toEmail,
                "TemplateAlias": "welcome",
                "TemplateModel": {
                  "name": toName,
                  "action_url": "https://cfps.dev/home/talks",
                  "login_url": "https://cfps.dev/home/",
                  "username": toEmail,
                  "product_name": "CFPs.dev"
                }
              }).catch(e => {
                console.log("Error sending email");
                console.error(e);
              })

            const data = { userId, emailType: "welcome" }
            const response = await dbClient.query(Create(Collection("emails"), { data }));
            console.log('Success adding record', response);
            return {
                statusCode: 200,
                body: JSON.stringify(response)
            }
        }
        console.log('error', error)
        return true;
    }
}

module.exports = { welcome }