const getTalks = require("./get");
const create = require("./create");
const updateTalks = require("./update");
const deleteTalks = require("./delete");

const handler = async (event, context) => {
  console.log("Talks API");

  try {
    const path = event.path.replace(/\.netlify\/functions\/[^/]+/, "");
    const segments = path.split("/").filter(Boolean);
    const { user, identity } = context.clientContext;
    let userId = "";
    if (user) {
      if (user.sub) {
        userId = user.sub;
      }
    }
    switch (event.httpMethod) {
      case "GET":
        if (segments.length === 0) return getTalks.all(userId);
        if (segments.length === 1) {
          const [id] = segments;
          return getTalks.one(userId, id);
        }
        if (segments.length === 2) {
          try {
            console.log("Two segments", segments);
            const [dir, id] = segments;
            switch (dir) {
              case "public":
                console.log(`Getting all public talks for ${id}`);
                return getTalks.public(id);
              default:
                return {
                  statusCode: 404,
                  body: JSON.stringify({ message: "Not found dir"})
                };
            }
          } catch (error) {
            console.log(error);
            return {
              statusCode: 400,
              body: JSON.stringify(error),
            };
          }
        }
        return {
          statusCode: 400,
          body: JSON.stringify({ message: "Too many segments in GET request.  Either / or /:id or /public/:shortCode required" })
        };
      case "POST":
        return create.newTalk(event, userId);
      case "PUT":
        if (segments.length === 1) {
          const [id] = segments;
          return updateTalks.one(userId, id, event);
        }
        return {
          statusCode: 400,
          body: JSON.stringify({ message: "Invalid segments.  Must be of the form /:id" })
        };
      case "DELETE":
        if (segments.length === 1) {
          const [id] = segments;
          return deleteTalks.one(userId, id);
        }
        return {
          statusCode: 400,
          body: JSON.stringify({ message: "Invalid segments.  Must be of the form /:id" })
        };
      default:
        console.log("Where is my large automobile?");
        break;
    }
  } catch (error) {
    console.log("General error in function");
    console.log("error", error);
    return {
      statusCode: 500,
      body: `There was an error ${error}`,
    };
  }
};

module.exports = { handler };
