const process = require('process');

const { query, Create, Collection, Paginate, Select, Get, Client, Call, Function, If, Update, Match, Index, Ref } = require('faunadb')

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const setShortCode = async (event, userId) => {
    const data = JSON.parse(event.body);
    const { shortCode } = data;
    console.log('Function setShortCode', data);
    const item = {
        data: { ...data, userId }
    }

    try {
        const shortCodeInUse = await client.query(Call(Function("shortCodeExists"), shortCode));
        if (shortCodeInUse) {
            return {
                statusCode: 400,
                body: JSON.stringify({ message: `Shortcode ${shortCode} is already in use` })
            }
        } else {
            const response = await client.query(
                If(
                    Call(Function("userIdHasShortCode"), userId),
                    Update(Select(["ref"], Get(Match(Index("user_profiles_userid"), userId))), item),
                    Create(Collection("user_profiles"), item)
                )
            );
            console.log("Successful update", response);
            return {
                statusCode: 200,
                body: JSON.stringify(response)
            }
        }
    } catch (error) {
        console.error(error);
        return {
            statusCode: 400,
            body: JSON.stringify(error)
        }
    }
}

module.exports = { setShortCode }