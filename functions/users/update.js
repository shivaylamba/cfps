const process = require('process');

const { Get, Client, Select, Match, Index, Ref, Collection, Update, Now, Create } = require('faunadb')

const client = new Client({
    secret: process.env.FAUNADB_SERVER_SECRET,
})

const one = async (userId, event) => {
    const data = JSON.parse(event.body);
    console.log(`User ${userId} profile update`);

    try {
        const thisRef = await client.query(
            Select(["ref"], Get(Match(Index("user_profiles_userid"), userId)))
        );
        const thisProfile = await client.query(
            Get(Match(Index("user_profiles_userid"), userId))
        );

        if (thisProfile.data.userId === userId) {
            const response = await client.query(Update(thisRef, { data }));
            console.log('success', response);
            return {
                statusCode: 200,
                body: JSON.stringify(response)
            }
        } else {
            return {
                statusCode: 401,
                body: JSON.stringify({ message: `Incorrect user ID with user ${userId} on update profile` })
            }
        }
    } catch (error) {
        console.log('error', error)
        return {
            statusCode: 400,
            body: JSON.stringify(error),
        }
    }
}

const login = async (userId, event) => {
    const data = JSON.parse(event.body);
    console.log(`User ${userId} login update`);
    const item = {
        data: { ...data, userId, lastLogin: Now() }
    }

    try {
        const thisRef = await client.query(
            Select(["ref"], Get(Match(Index("users_userid"), userId)))
        );

        const thisProfile = await client.query(
            Get(Match(Index("users_userid"), userId))
        );
        if (thisProfile.data.userId === userId) {
            const response = await client.query(Update(thisRef, { data: item }));
            return {
                statusCode: 200,
                body: JSON.stringify(response)
            }
        } else {
            return {
                statusCode: 401,
                body: JSON.stringify({ message: `Incorrect user ID with user ${userId} on update profile` })
            }
        }
    } catch (error) {
        if (error.requestResult.statusCode === 404) {
            const response2 = await client.query(Create(Collection("users"), item));
            console.log('Success adding record', response2);
            return {
                statusCode: 200,
                body: JSON.stringify(response2)
            }
        }
        console.log('error', error)
        return {
            statusCode: 400,
            body: JSON.stringify(error),
        }
    }
}

module.exports = { one, login }