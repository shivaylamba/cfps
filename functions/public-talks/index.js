const getTalks = require('./get');

const handler = async (event, context) => {
    const path = event.path.replace(/\.netlify\/functions\/[^/]+/, '')
    const segments = path.split('/').filter(Boolean);

    try {
        // const { user, identity } = context.clientContext;
        // const userId = user.sub;

        switch (event.httpMethod) {
            case 'GET':
                if (segments.length === 0) {
                    return {
                        statusCode: 400,
                        body: JSON.stringify({ message: 'Requires a user name' })
                    }
                }
                if (segments.length === 1) {
                    const [id] = segments;
                    return getTalks.public(id);
                }
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: 'Too many segments in GET request.  Either /:id required'})
                }
                break;

            default:
                break;
        }
    } catch (error) {
        console.log('error', error);
        return {
            statusCode: 500,
            body: JSON.stringify(error),
        }
    }
}

module.exports = { handler }