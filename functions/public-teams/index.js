const getTeam = require('./get');
const addToTeam = require('./add');

const handler = async (event, context) => {
    const path = event.path.replace(/\.netlify\/functions\/[^/]+/, '')
    const segments = path.split('/').filter(Boolean);

    try {
        const { user, identity } = context.clientContext;
        let userId = "";
        if (user) {
          if (user.sub) {
            userId = user.sub;
          }
        }

        switch (event.httpMethod) {
            case 'GET':
                if (segments.length === 0) {
                    return {
                        statusCode: 400,
                        body: JSON.stringify({ message: 'Requires a invite id' })
                    }
                }
                if (segments.length === 1) {
                    const [id] = segments;
                    return getTeam.public(id);
                }
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: 'Too many segments in GET request.  Either /:id required'})
                }
            case 'PUT':
                if (segments.length === 0) {
                    return {
                        statusCode: 400,
                        body: JSON.stringify({ message: 'Requires a invite id' })
                    }
                }
                if (segments.length === 1) {
                    const [id] = segments;
                    return addToTeam.acceptInvite(userId, id);
                }
                return {
                    statusCode: 400,
                    body: JSON.stringify({ message: 'Too many segments in GET request.  Either /:id required'})
                }
            default:
                break;
        }
    } catch (error) {
        console.log('error', error);
        return {
            statusCode: 500,
            body: JSON.stringify(error),
        }
    }
}

module.exports = { handler }