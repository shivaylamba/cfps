# CFPs.dev Database  

## Collections

### `cfps`

```json
{
    "userId": "0d943a5f-ca11-4140-b713-1faf5379120b",
    "event": "Linux Foundation Member Summit",
    "duedate": "2021-08-08",
    "eventstart": "2021-11-02",
    "eventend": "2021-11-04",
    "location": "Napa, CA",
    "virtual": false,
    "attendance": "Planned",
    "cfpstatus": "Submitted",
    "eventURL": "https://example.com",
    "cfpURL": "https://example.com",
    "submissions": [
      {
        talkRef: "3214568765143",
        status: "Submitted"
      }
    ],
    "topics": [
      "Open Source",
      "Linux Foundation"
    ],
}
```

### `talks`
```json
{
    "userId": "0d943a5f-ca11-4140-b713-1faf5379120b",
    "title": "Don't build a toaster",
    "icon": "🗣️",
    "tagline": "",
    "abstract": "",
    "status": "Planned",
    "topics": [
      "Open Source",
      "Linux Foundation"
    ]
}
```

### `submissons`
```json
{
    "cfpRef": "123456789",
    "talkRef": "987654321",
    "status": "Rejected"
}
```

## Indexes

### `all_cfps`
```json
{
  name: "all_cfps",
  unique: false,
  serialized: true,
  source: "cfps"
}
```

### `user_cfps`
```json
{
  name: "user_cfps",
  unique: false,
  serialized: true,
  source: "cfps",
  terms: [
    {
      field: ["data", "userId"]
    }
  ]
}
```

### `user_talks`
```json
{
  name: "user_talks",
  unique: false,
  serialized: true,
  source: "talks",
  terms: [
    {
      field: ["data", "userId"]
    }
  ]
}
```

### `teams`

```json
{
    "creatorId": "0d943a5f-ca11-4140-b713-1faf5379120b",
    "teamname": "Linux Foundation",
    "stripeCustId": "ABCD-1234",
    "subscribed": false,
    "owners": [
      "0d943a5f-ca11-4140-b713-1faf5379120b"
    ],
    "members": [
      "0d943a5f-ca11-4140-b713-1faf5379120b",
    ],
}
```

### `emails`

- userId
- emailType
- details

```json
tbd
```