// DATA from running "Map(Paginate(Indexes()), Lambda("x", Get(Var("x"))))" on Prod
const { Collection, Index } = require('faunadb');

const indexData = [
    {
        ref: Index("users_by_id"),
        ts: 1628009189800000,
        active: true,
        serialized: true,
        name: "users_by_id",
        source: Collection("users"),
        terms: [
            {
                field: ["data", "id"]
            }
        ],
        unique: true,
        partitions: 1
    },
    {
        ref: Index("all_users"),
        ts: 1628009189800000,
        active: true,
        serialized: true,
        name: "all_users",
        source: Collection("users"),
        partitions: 8
    },
    {
        ref: Index("all_cfps"),
        ts: 1628045249110000,
        active: true,
        serialized: true,
        name: "all_cfps",
        unique: false,
        source: Collection("cfps"),
        partitions: 8
    },
    {
        ref: Index("user_cfps"),
        ts: 1628047650590000,
        active: true,
        serialized: true,
        name: "user_cfps",
        unique: false,
        source: Collection("cfps"),
        terms: [
            {
                field: ["data", "userId"]
            }
        ],
        partitions: 1
    },
    {
        ref: Index("user_talks"),
        ts: 1629145119873000,
        active: true,
        serialized: true,
        name: "user_talks",
        unique: false,
        source: Collection("talks"),
        terms: [
            {
                field: ["data", "userId"]
            }
        ],
        partitions: 1
    },
    {
        ref: Index("talks_by_userId"),
        ts: 1629145152550000,
        active: true,
        serialized: true,
        name: "talks_by_userId",
        unique: false,
        source: Collection("talks"),
        terms: [
            {
                field: ["data", "userId"]
            }
        ],
        partitions: 1
    },
    {
        ref: Index("user_profiles_userid"),
        ts: 1629835277305000,
        active: true,
        serialized: true,
        name: "user_profiles_userid",
        unique: true,
        source: Collection("user_profiles"),
        terms: [
            {
                field: ["data", "userId"]
            }
        ],
        partitions: 1
    },
    {
        ref: Index("user_profiles_shortcode"),
        ts: 1629836501210000,
        active: true,
        serialized: true,
        name: "user_profiles_shortcode",
        unique: true,
        source: Collection("user_profiles"),
        terms: [
            {
                field: ["data", "shortCode"]
            }
        ],
        partitions: 1
    },
    {
        ref: Index("published_talks_by_userId"),
        ts: 1630759306710000,
        active: true,
        serialized: true,
        name: "published_talks_by_userId",
        unique: false,
        source: Collection("talks"),
        terms: [
            {
                field: ["data", "published"]
            },
            {
                field: ["data", "userId"]
            }
        ],
        partitions: 1
    },
    {
        ref: Index("talks_by_slug"),
        ts: 1631621980220000,
        active: true,
        serialized: true,
        name: "talks_by_slug",
        unique: false,
        source: Collection("talks"),
        terms: [
            {
                field: ["data", "slug"]
            }
        ],
        partitions: 1
    },
    {
        ref: Index("talks_slug_userId"),
        ts: 1631622028185000,
        active: true,
        serialized: true,
        name: "talks_slug_userId",
        unique: false,
        source: Collection("talks"),
        terms: [
            {
                field: ["data", "slug"]
            },
            {
                field: ["data", "userId"]
            }
        ],
        partitions: 1
    }
]

module.exports = { indexData }