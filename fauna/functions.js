// DATA from running "Map(Paginate(Functions()), Lambda("x", Get(Var("x"))))" on Prod
const { Ref, Lambda, Query, Exists, Match, Var, Index, Select, Paginate, Get } = require('faunadb');

const functionDetails = [
    {
        ref: Ref(Ref("functions"), "shortCodeExists"),
        ts: 1629834956350000,
        name: "shortCodeExists",
        body: Query(
            Lambda(
                ["shortCode"],
                Exists(Match(Index("user_profiles_shortcode"), Var("shortCode")))
            )
        )
    },
    {
        ref: Ref(Ref("functions"), "userIdHasShortCode"),
        ts: 1629835342630000,
        name: "userIdHasShortCode",
        body: Query(
            Lambda(
                ["userId"],
                Exists(Match(Index("user_profiles_userid"), Var("userId")))
            )
        )
    },
    {
        ref: Ref(Ref("functions"), "userId_from_shortCode"),
        ts: 1630762254015000,
        name: "userId_from_shortCode",
        body: Query(
            Lambda(
                "shortCode",
                Select(
                    "userId",
                    Select(
                        "data",
                        Get(
                            Select(
                                0,
                                Paginate(
                                    Match(Index("user_profiles_shortcode"), Var("shortCode"))
                                )
                            )
                        )
                    )
                )
            )
        )
    }
]

module.exports = { functionDetails }