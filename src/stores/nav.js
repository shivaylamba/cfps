export const topLevelNav = [
    {
        path: '/home',
        longName: 'CFP Dashboard',
        shortName: 'CFPs',
        icon: '🗣️',
        headerText: 'CFPs',
        navKey: 'cfps'
    },
    {
        path: '/home/teams',
        longName: 'Teams Home',
        shortName: 'Teams',
        icon: '⚽',
        headerText: 'Teams',
        navKey: 'teams'
    },
    {
        path: '/home/events',
        longName: 'Event Dashboard',
        shortName: 'Events',
        icon: '📆',
        headerText: 'Events you\'re attending',
        navKey: 'events'
    },
    {
        path: '/home/talks',
        longName: 'Talk Abstract Dashboard',
        shortName: 'Abstracts',
        icon: '🎤',
        headerText: 'Abstract Inventory',
        navKey: 'talks'
    },
]

export const mainNav = {
    cfps: [
        {
            path: '/home/due',
            shortName: 'Due Soon'
        },
        {
            path: '/home/planned',
            shortName: 'Planned'
        },
        {
            path: '/home/submitted',
            shortName: 'Submitted'
        },
        {
            path: '/home/accepted',
            shortName: 'Accepted'
        },
        {
            path: '/home/all',
            longName: 'All CFPs',
            shortName: 'All'
        }
    ],
    teams: [

    ],
    events: [
        {
            path: '/home/events/calendar',
            shortName: 'Calendar'
        },
        {
            path: '/home/events/upcoming',
            shortName: 'Upcoming'
        },
        {
            path: '/home/events/past',
            shortName: 'Past'
        },
        {
            path: '/home/events/all',
            shortName: 'All Events'
        },
    ],
    talks: [
        {
            path: '/home/talks/drafts',
            shortName: 'Drafts'
        },
        {
            path: '/home/talks/published',
            shortName: 'Published'
        },
        {
            path: '/home/talks/all',
            shortName: 'All Abstracts'
        },
    ]
}