const url = "/.netlify/functions/teams";

function headers(user) {
  return {
    Authorization: `Bearer ${user.token.access_token}`,
  };
}

export async function getAll(user) {
  return fetch(url, {
    method: "GET",
    headers: headers(user),
  }).then((response) => {
    return response.json();
  });
}


export async function getOne(user, ref) {
  return fetch(`${url}/${ref}`, {
    method: "GET",
    headers: headers(user),
  }).then((response) => {
    return response.json();
  });
}

export async function getCFPs(user, ref) {
  return fetch(`${url}/${ref}/cfps`, {
    method: "GET",
    headers: headers(user),
  }).then((response) => {
    return response.json();
  });
}

export async function add(user, data) {
  return fetch(url, {
    method: "POST",
    body: JSON.stringify(data),
    headers: headers(user),
  });
}

export async function update(user, ref, data) {
  return fetch(`${url}/${ref}`, {
    method: 'PUT',
    body: JSON.stringify(data),
    headers: headers(user)
  }).then(response => { return response.json() })
}

export async function deleteOne(user, ref) {
  return fetch(`${url}/${ref}`, {
    method: 'DELETE',
    headers: headers(user)
  })
}

export async function getPublic(shortCode) {
  return fetch(`${url}/${shortCode}/public`, {
    method: "GET"
  }).then(response => { return response.json() })
}

