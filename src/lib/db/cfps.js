import dayjs from "dayjs";
//import relativeTime from 'dayjs/plugin/relativeTime'
//dayjs.extend(relativeTime);

const url = "/.netlify/functions/cfps";

function headers(user) {
  return {
    Authorization: `Bearer ${user.token.access_token}`,
  };
}

export async function getAll(user) {
  return fetch(url, {
    method: "GET",
    headers: headers(user),
  }).then((response) => {
    return response.json();
  });
}

export const planned = (arr) => { return arr.filter(cfp => cfp.cfpstatus === "Planned") }
export const submitted = (arr) => { return arr.filter(cfp => cfp.cfpstatus === "Submitted") }
export const accepted = (arr) => { return arr.filter(cfp => cfp.cfpstatus === "Accepted") }

export function duesoon(arr) {
  arr.forEach(element => {
    if (!element.dueInDays && element.data) { element.dueInDays = dayjs(element.data.duedate).diff(dayjs(), 'day') }
  });
  return arr.filter(cfp => cfp.dueInDays > -1 && cfp.dueInDays < 7)
}

function addDT(arr) {
  let ret = [];
  arr.map((cfp) => {
    ret.push({
      ...cfp.data,
      dueInDays: dayjs(cfp.data.duedate).diff(dayjs(), "day"),
    });
  });
  return ret;
}

export async function getStats(user) {
  let allCFPs = [];
  allCFPs = await getAll(user);
  let retCFPs = addDT(allCFPs);

  return {
    planned: planned(retCFPs).length,
    submitted: submitted(retCFPs).length,
    accepted: accepted(retCFPs).length,
    duesoon: duesoon(retCFPs).length,
    allCFPs: sortList(allCFPs, 'duedate')
  }
}

export async function getOne(user, ref) {
  return fetch(`${url}/${ref}`, {
    method: "GET",
    headers: headers(user),
  }).then((response) => {
    return response.json();
  });
}

export async function add(user, data) {
  return fetch(url, {
    method: "POST",
    body: JSON.stringify(data),
    headers: headers(user),
  });
}

export async function update(user, ref, data) {
  return fetch(`${url}/${ref}`, {
    method: 'PUT',
    body: JSON.stringify(data),
    headers: headers(user)
  }).then(response => { return response.json() })
}

export async function deleteOne(user, ref) {
  return fetch(`${url}/${ref}`, {
    method: 'DELETE',
    headers: headers(user)
  })
}

export async function getPublic(shortCode) {
  return fetch(`${url}/${shortCode}/public`, {
    method: "GET"
  }).then(response => { return response.json() })
}

/**
 * 
 * @param {array} myCFPs Array of unsorted CFPs
 * @param {string} field Field to sort on (e.g. 'duedate', 'eventstart', etc.)
 * @returns {array} Sorted list of CFPs
 */
export function sortList(myCFPs, field) {
  return myCFPs.sort((a, b) => (a.data[field] > b.data[field] ? 1 : -1));
}
