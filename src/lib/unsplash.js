import { headers } from "./db/utils";

const url = "/.netlify/functions/unsplash";

export async function search(str, user) {
  return fetch(`${url}?query=${str}`, {
    method: "GET",
    headers: headers(user),
  }).then((response) => {
    return response.json();
  });
}

export async function select(picId, user) {
  return fetch(`${url}/${picId}`, {
    method: "GET",
    headers: headers(user),
  }).then((response) => {
    return response.json();
  });
}
