import App from "./App.svelte";

import { login, logout } from "./stores/user";

const dev = !!import.meta.hot;

const app = new App({
  target: document.querySelector("#content"),
  hydrate: !dev
});

if (dev) {
  import.meta.hot.dispose(() => {
    app.$destroy();
  });
  import.meta.hot.accept();
}

netlifyIdentity.on('init', user => console.log('init', user));
netlifyIdentity.on('login', login);
netlifyIdentity.on('logout', logout);

// #14: Catch errors
netlifyIdentity.on('error', err => console.error('Error', err));
netlifyIdentity.on('open', () => console.log('Widget opened'));
netlifyIdentity.on('close', () => console.log('Widget closed'));

netlifyIdentity.init();

if (netlifyIdentity.currentUser()) {
  login(netlifyIdentity.currentUser(), false)
} else {
  logout(false)
}


export default app;
